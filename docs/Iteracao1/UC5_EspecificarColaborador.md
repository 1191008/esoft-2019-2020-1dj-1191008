# UC Especificar Colaborador de Organização

## 1. Engenharia de Requisitos

### Formato Breve

O ator pretende especificar um colaborador relativo a uma organização registada. O sistema solicita dados necessários (i.e. nome do colaborador e uma breve descrição). O administrativo introduz os dados necessários. O sistema valida e apresenta novamente os dados introduzidos, esperando uma confirmação do administrativo. O administrativo confirma. O sistema regista os dados e confirma o seu sucesso ao administrativo.

### SSD
![UCXX-SSD](SSD_UCXX.png)


### Formato Completo

#### Ator principal

Gestor de Organização

#### Partes interessadas e seus interesses

* **Gestor da Organização:** pretende determinar um colaborador para a sua organização.
* **T4J:** pretende ter seus colaboradores e organizações devidamente especificados e organizados.

#### Pré-condições

A organização e o colaborador especificado estarem registados no sistema.

#### Pós-condições

O colaborador especificado para a organização ficar guardado no sistema.

#### Cenário de sucesso principal (ou fluxo básico)

1. O ator inicia o caso de uso. 
2. O sistema solicita os dados necessários (i.e nome do colaborador, nome da organização, códigos de registo). 
3. O ator introduz os dados. 
4. O sistema valida e pede uma confirmação ao ator. 
5. O ator confirma os dados novamente. 
6. O sistema guarda o colaborador associado à organização. 


#### Extensões (ou fluxos alternativos)

*a. O gestor da organização solicita o cancelamento da especificação do colaborador.

> O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.
>	1. O sistema informa quais os dados em falta.
>	2. O sistema permite a introdução dos dados em falta (passo 3)
>
	>	2a. O gestor da organização não altera os dados. O caso de uso termina.

4b. O sistema deteta que os dados (ou algum subconjunto dos dados) introduzidos devem ser únicos e que já existem na área de trabalho.
>	1. O sistema alerta o administrativo para o facto.
>	2. O sistema permite a sua alteração (passo 3)
>
	>	2a. O gestor da organização não altera os dados. O caso de uso termina.

4c. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.
> 1. O sistema alerta o administrativo para o facto. 
> 2. O sistema permite a sua alteração (passo 3).
> 
	> 2a. O gestor da organização não altera os dados. O caso de uso termina. 

#### Requisitos especiais

\-

#### Lista de Variações de Tecnologias e Dados

\-

#### Frequência de Ocorrência

\-

#### Questões em aberto

* Os colaboradores associados a uma organização deverão estar registados previamente no sistema?
* Quais dados adicionais seriam necessários paralém dos mencionados anteriormente?
* Existe algum dado que não deve ser duplicado no sistema?
* Todos os dados são necessários para o cumprimento deste caso de uso?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

(Apresentar aqui um excerto do MD relevante para este UC)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| Passo1  		 |							 |             |                              |
| Passo2  		 |							 |             |                              |
| Passo3  		 |							 |             |                              |
| Passo4  		 |							 |             |                              |
| Passo5  		 |							 |             |                              |
| Passo6  		 |							 |             |                              |              

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Classe1
 * Classe2
 * Classe3

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * xxxxUI  
 * xxxxController


###	Diagrama de Sequência

![SD_UCX.png](SD_UCX.png)


###	Diagrama de Classes

![CD_UCX.png](CD_UCX.png)
