# UC4 Especificar Competência Técnica

## 1. Engenharia de Requisitos

### Formato Breve

O Administrativo pretende definir as diferentes competências técnicas relativas às diferentes categorias de trabalho.O  sistema solicita dados necessários (i.e. nome da CT e uma breve descrição). O administrativo introduz os dados necessários. O sistema valida e apresenta novamente os dados introduzidos, esperando uma confirmação do administrativo. O administrativo confirma. O sistema regista os dados e confirma o seu sucesso ao administrativo.

### SSD
![UC4-SSD](SSD_UC4.png)


### Formato Completo

#### Ator principal

Administrativo

#### Partes interessadas e seus interesses

* **Administrativo:** pretende determincar uma competência técnica de uma certa categoria de tarefa.

#### Pré-condições

Áreas de trabalho estabelecidas e categorias de tarefas definidas

#### Pós-condições

Competência técnica estabelecida e guardada no sistema

#### Cenário de sucesso principal (ou fluxo básico)

1. O ator inicia a especificação de uma competência técnica. 
2. O sistema solicita os dados necessários da competência (i.e. tarefas envolvidas, nome da competência, descrição breve e detalhada). 
3. O ator introduz os dados necessários. 
4. O sistema valida os dados, apresentando-os e pedindo uma confirmação. 
5. O ator confirma os dados. 
6. O sistema regista a competência técnica. 

#### Extensões (ou fluxos alternativos)

*a. O ator executa o cancelamento do processo.

> O caso de uso termina.

4a. O sistema deteta um ou mais parâmetros obrigatórios não preenchidos.
>       1. O sistema não valida os dados introduzidos.
>       2. O sistema solicita a reintrodução dos dados necessários.

            2a. O ator não altera os seus dados; o caso de uso termina.

4b. O sistema deteta que um parâmetro especifico deve ser único e já foi introduzido noutra competência técnica.
>       1. O sistema não valida os dados introduzidos.
>       2. O sistema solicita a reintrodução dos dados necessários.

>           2a. O ator não altera os seus dados; o caso de uso termina.

4c. O sistema deteta que um ou mais dados introduzidos é inválido.
>       1. O sistema não valida os dados introduzidos.
>       2. O sistema solicita a reintrodução dos dados necessários.

>           2a. O ator não altera os seus dados; o caso de uso termina.

#### Requisitos especiais
(enumerar requisitos especiais aplicaveis apenas a este UC)

\-

#### Lista de Variações de Tecnologias e Dados
(enumerar variações de tecnologias e dados aplicaveis apenas a este UC)
\-

#### Frequência de Ocorrência
(descrever com que frequência este UC é realizado)

\-

#### Questões em aberto

* Existem mais dados requerentes para efetuar o caso de uso?
* Quais os dados requeridos que não devem estar duplicados no sistema?
* É necessário existir algum mecanismo de segurança adicional para confirmar que a organização existe e é representada pela pessoa que a registou?
* Quais são as regras de segurança aplicaveis à palavra-passe?
* Qual a frequência de ocorrência deste caso de uso?
* As competências técnicas serão pré-determinadas, deixando o ator escolher de uma lista pré-feita, ou, alternativamente, as CT serão completamente customizáveis?
* A quem afetam as competências técnicas após serem estabelecidas numa categoria de tarefa/ área de trabalho?

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

(Apresentar aqui um excerto do MD relevante para este UC)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| Passo1  		 |							 |             |                              |
| Passo2  		 |							 |             |                              |
| Passo3  		 |							 |             |                              |
| Passo4  		 |							 |             |                              |
| Passo5  		 |							 |             |                              |
| Passo6  		 |							 |             |                              |              

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Classe1
 * Classe2
 * Classe3

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * xxxxUI  
 * xxxxController


###	Diagrama de Sequência

![SD_UCX.png](SD_UCX.png)


###	Diagrama de Classes

![CD_UCX.png](CD_UCX.png)
