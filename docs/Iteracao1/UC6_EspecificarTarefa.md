# UC Especificar Tarefa

## 1. Engenharia de Requisitos

### Formato Breve

O colaborador da organização pretende especificar uma tarefa numa categoria pré-determinado pelo administrativo. O sistema solicita dados necessários (i.e. nome da tarefa e uma breve descrição). O administrativo introduz os dados necessários. O sistema valida e apresenta novamente os dados introduzidos, esperando uma confirmação do administrativo. O administrativo confirma. O sistema regista os dados e confirma o seu sucesso ao administrativo.

### SSD
![UCXX-SSD](SSD_UCXX.png)


### Formato Completo

#### Ator principal

Colaborador de Organização

#### Partes interessadas e seus interesses

* **Colaborador da organização:** pretende determinar uma tarefa.

#### Pré-condições

Colaborador previamente definido pelo gestor da organização.
Categorias de tarefa e áreas de trabalho definidas.

#### Pós-condições

Tarefas guardadas no sistema e associadas à categoria correta.

#### Cenário de sucesso principal (ou fluxo básico)

1. O ator pretende definir uma nova tarefa. 
2. O sistema requer ao ator os dados necessários da tarefa (i.e nome, competências técnicas associadas, etc). 
3. O ator introduz os dados necessários.
4. O sistema valida os dados e solicita a confirmação do ator. 
5. O ator confirma os dados. 
6. O sistema regista a tarefa na categoria designada. 


#### Extensões (ou fluxos alternativos)

(alternativas globais ao fluxo principal ou especifica de um determiando passo)

#### Requisitos especiais
(enumerar requisitos especiais aplicaveis apenas a este UC)

\-

#### Lista de Variações de Tecnologias e Dados
(enumerar variações de tecnologias e dados aplicaveis apenas a este UC)
\-

#### Frequência de Ocorrência
(descrever com que frequência este UC é realizado)

\-

#### Questões em aberto

(lista de questões em aberto, i.e. sem uma resposta conhecida.)

## 2. Análise OO

### Excerto do Modelo de Domínio Relevante para o UC

(Apresentar aqui um excerto do MD relevante para este UC)


## 3. Design - Realização do Caso de Uso

### Racional

| Fluxo Principal | Questão: Que Classe... | Resposta  | Justificação  |
|:--------------  |:---------------------- |:----------|:---------------------------- |
| Passo1  		 |							 |             |                              |
| Passo2  		 |							 |             |                              |
| Passo3  		 |							 |             |                              |
| Passo4  		 |							 |             |                              |
| Passo5  		 |							 |             |                              |
| Passo6  		 |							 |             |                              |              

### Sistematização ##

 Do racional resulta que as classes conceptuais promovidas a classes de software são:

 * Classe1
 * Classe2
 * Classe3

Outras classes de software (i.e. Pure Fabrication) identificadas:  

 * xxxxUI  
 * xxxxController


###	Diagrama de Sequência

![SD_UCX.png](SD_UCX.png)


###	Diagrama de Classes

![CD_UCX.png](CD_UCX.png)
